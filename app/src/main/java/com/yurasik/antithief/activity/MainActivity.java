package com.yurasik.antithief.activity;

import android.app.Activity;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.widget.CompoundButton;
import android.widget.ToggleButton;

import com.yurasik.antithief.Constants;
import com.yurasik.antithief.R;
import com.yurasik.antithief.broadcast.BootUpReceiver;
import com.yurasik.antithief.service.AntithiefService;

public class MainActivity extends Activity {

    private ToggleButton tgAntiThiefStatus;
    private ToggleButton tgAutoexec;
    private SharedPreferences preferences;
    private BootUpReceiver bootUpReceiver;
    private boolean status, autoLoad = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        findView();

        preferences = PreferenceManager.getDefaultSharedPreferences(this);
        status = preferences.getBoolean(Constants.ANTI_THIEF_STATUS, false);
        autoLoad = preferences.getBoolean(Constants.AUTOEXEC_STATUS, false);

        tgAntiThiefStatus.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                preferences.edit().putBoolean(Constants.ANTI_THIEF_STATUS, b).commit();
                if (b) {
                    startLockService();
                    tgAutoexec.setEnabled(true);
                } else {
                    stopLockService();
                    tgAutoexec.setEnabled(false);
                }
            }
        });

        if (!tgAntiThiefStatus.isChecked()) {
            tgAutoexec.setEnabled(false);
            tgAntiThiefStatus.setChecked(false);
        } else {
            tgAutoexec.setChecked(preferences.getBoolean(Constants.AUTOEXEC_STATUS, false));
        }
        tgAutoexec.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                preferences.edit().putBoolean(Constants.AUTOEXEC_STATUS, b).commit();
                changeReceiver(b);
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        tgAntiThiefStatus.setChecked(status);
        tgAutoexec.setChecked(autoLoad);
    }

    private void startLockService() {
        Intent intent = new Intent(this, AntithiefService.class);
        intent.putExtra(Constants.SERVICE_COMMAND, Constants.COMMAND_START_SERVICE);
        startService(intent);
    }

    private void stopLockService() {
        Intent intent = new Intent(this, AntithiefService.class);
        intent.putExtra(Constants.SERVICE_COMMAND, Constants.COMMAND_STOP_SERVICE);
        startService(intent);
    }

    private void changeReceiver(boolean b) {
        if (b) {
            IntentFilter filter = new IntentFilter("android.intent.action.USER_PRESENT");
            bootUpReceiver = new BootUpReceiver();
            registerReceiver(bootUpReceiver, filter);
        } else {
            if (bootUpReceiver != null) {
                unregisterReceiver(bootUpReceiver);
                bootUpReceiver = null;
            }
        }
    }

    private void findView() {
        tgAntiThiefStatus = ToggleButton.class.cast(findViewById(R.id.tgLockStatus));
        tgAutoexec = ToggleButton.class.cast(findViewById(R.id.tgAutoexec));
    }


}
