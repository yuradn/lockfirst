package com.yurasik.antithief.service;

import android.app.Notification;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.PixelFormat;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

import com.yurasik.antithief.Constants;
import com.yurasik.antithief.R;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.concurrent.TimeUnit;

/**
 * Created by NewUser on 9/2/16.
 */

public class AntithiefService extends Service {
    private final static String TAG = "Service";
    public static boolean IS_RUNNING = false;
    public static boolean IS_LOCKSCREEN = false;

    private MyTask myTask;

    WindowManager.LayoutParams paramsBack;
    private WindowManager windowManager;
    public ViewGroup backLayout;

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    // Register for Lockscreen event intents
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent != null && intent.hasExtra(Constants.SERVICE_COMMAND)) {
            String command = intent.getStringExtra(Constants.SERVICE_COMMAND);
            switch (command) {
                case Constants.COMMAND_START_LOCK_SCREEN:
                    lock();
                case Constants.COMMAND_START_SERVICE:
                    if (IS_RUNNING) return super.onStartCommand(intent, flags, startId);
                    IS_RUNNING = true;
                    startForeground();
                    startJob();
                    break;
                case Constants.COMMAND_STOP_SERVICE:
                    if (!IS_RUNNING) return super.onStartCommand(intent, flags, startId);
                    IS_RUNNING = false;
                    stopForeground(true);
                    stopSelf();
                    break;
            }
        }

        return super.onStartCommand(intent, flags, startId);
    }

    // Run service in foreground so it is less likely to be killed by system
    private void startForeground() {
        Notification notification = new Notification.Builder(this)
                .setContentTitle(getResources().getString(R.string.app_name))
                .setTicker(getResources().getString(R.string.app_name))
                .setContentText("Anti thief is running")
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentIntent(null)
                .setOngoing(true)
                .build();
        startForeground(9999, notification);
    }

    private void startJob() {
        if (myTask == null) {
            myTask = new MyTask();
            myTask.execute();
        }
    }

    private boolean isNetwork() {
        ConnectivityManager connMgr = (ConnectivityManager)
                getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected()) {
            return true;
            //new DownloadWebpageTask().execute(stringUrl);
        } else {
            Log.e(TAG, "No network connection available.");
            return false;
        }
    }

    private boolean isAlarm() {
        URL url = null;
        try {
            url = new URL(Constants.URL);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.connect();
            InputStream is = conn.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
            String webPage = "", data = "";

            while ((data = reader.readLine()) != null) {
                webPage += data + "\n";
            }

            Log.d(TAG, "Web: " + webPage + " size: " + webPage.length());
            conn.disconnect();
            int o = Integer.parseInt(webPage.trim());
            return o == 0;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }

    // Unregister receiver
    @Override
    public void onDestroy() {
        super.onDestroy();
        IS_RUNNING = false;
    }


    class MyTask extends AsyncTask<Void, Boolean, Void> {

        @Override
        protected Void doInBackground(Void... voids) {
            while (IS_RUNNING) {
                try {
                    Thread.sleep(TimeUnit.SECONDS.toMillis(3));
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                if (isNetwork()) {
                    boolean security = isAlarm();
                    Log.d(TAG, "Alarm flag: " + security + " lock status: " + IS_LOCKSCREEN);
                    if (security && !IS_LOCKSCREEN) {
                        AntithiefService.IS_LOCKSCREEN = true;
                        Boolean[] tmp = new Boolean[]{true};
                        publishProgress(tmp);
                    } else if (!security && AntithiefService.IS_LOCKSCREEN) {
                        IS_LOCKSCREEN = false;
                        Boolean[] tmp = new Boolean[]{false};
                        publishProgress(tmp);
                    }
                }
            }
            return null;
        }

        @Override
        protected void onProgressUpdate(Boolean... values) {
            super.onProgressUpdate(values);
            Log.d(TAG, " Values: " + values[0]);
            if (values[0]) {
                lock();
            } else {
                unLock();
            }

            SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
            preferences.edit().putBoolean(Constants.LOCK_STATUS, values[0]).commit();
        }
    }

    private void lock() {
        paramsBack = new WindowManager.LayoutParams(
                WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.TYPE_SYSTEM_OVERLAY,
                WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN,
                PixelFormat.TRANSLUCENT);

        windowManager = (WindowManager) getApplicationContext()
                .getSystemService(Context.WINDOW_SERVICE);

        if (backLayout != null) {
            windowManager.removeView(backLayout);
            backLayout = null;
        }

        LayoutInflater inflater = (LayoutInflater) getBaseContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        backLayout = (ViewGroup) inflater.inflate(R.layout.activity_lock, null);
        backLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d(TAG, "Click!");
            }
        });

        windowManager.addView(backLayout, paramsBack);
    }

    private void unLock() {
        windowManager.removeView(backLayout);
        backLayout = null;
    }

}
