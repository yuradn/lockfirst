package com.yurasik.antithief.broadcast;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import com.yurasik.antithief.Constants;
import com.yurasik.antithief.service.AntithiefService;

/**
 * Created by NewUser on 9/2/16.
 */

public class BootUpReceiver extends BroadcastReceiver {
    private final static String TAG = "AntiThiefBr";

    @Override
    public void onReceive(Context context, Intent intent) {

        if (intent.getAction().equals(Intent.ACTION_BOOT_COMPLETED)) {
            SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
            boolean lock = preferences.getBoolean(Constants.LOCK_STATUS, false);
            boolean autoexec = preferences.getBoolean(Constants.AUTOEXEC_STATUS,false);
            Log.d(TAG, "Lock status: " + lock+" Autoload: "+autoexec);
            if (lock) lockScreen(context);
            else {
                if (autoexec) {
                    startService(context);
                }
            }
        }

    }

    private void startService(Context context) {
        Intent intent = new Intent(context, AntithiefService.class);
        intent.putExtra(Constants.SERVICE_COMMAND, Constants.COMMAND_START_SERVICE);
        context.startService(intent);
    }

    private void lockScreen(Context context) {
        Intent intent = new Intent(context, AntithiefService.class);
        intent.putExtra(Constants.SERVICE_COMMAND, Constants.COMMAND_START_LOCK_SCREEN);
        context.startService(intent);
    }

}