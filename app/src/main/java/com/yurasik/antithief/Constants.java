package com.yurasik.antithief;

/**
 * Created by NewUser on 9/2/16.
 */

public class Constants {
    public static final String URL = "http://185.106.120.167/api/api.php";
    public static final String SERVICE_COMMAND = "com.yurasik.antithief.service";
    public static final String LOCK_STATUS = "com.yurasik.antithief.lock_status";
    public static final String AUTOEXEC_STATUS = "com.yurasik.antithief.autoload";
    public static final String COMMAND_START_SERVICE = "com.yurasik.antithief.start";
    public static final String COMMAND_STOP_SERVICE = "com.yurasik.antithief.stop";
    public static final String COMMAND_START_LOCK_SCREEN = "com.yurasik.antithief.service_lock";
    public static final String COMMAND_START_UNLOCK_SCREEN = "com.yurasik.antithief.service_unlock";
    public static final String ACTION_LOCK = "com.yurasik.antithief.lockscreen";
    public static final String ACTION_UNLOCK = "com.yurasik.antithief.unlockscreen";
    public static final String ANTI_THIEF_STATUS = "com.yurasik.antithief.anti_thief_status";
}
